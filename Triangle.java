public class Triangle {
    private double base;
    private double height; 

    //contructor
    public Triangle(double b, double h){
        this.base = b;
        this.height = h;
    }

    public double getArea(){
        return (this.base + this.height)/2;
    }
    public String toString(){
        return "Base: " + this.base + ", Height: " + this.height;
    }
    //get method
    public double getBase(){
        return this.base;
    }
    public double getHeight(){
        return this.height;
    }

}
