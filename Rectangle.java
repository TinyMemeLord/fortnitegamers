public class Rectangle {

    private int height;
    private int width;
    private int area;

    public Rectangle(int setHeight, int setWidth) {

        this.height = setHeight;
        this.width = setWidth;
        this.area = setHeight * setWidth;
    }

    public int getHeight() {

        return this.height;
    }

    public int getWidth() {

        return this.width;
    }

    public int getArea() {

        return this.area;
    }

    public String toString() {

        return "This rectangle has a height of " + this.height + " a width of " + this.width + " and an area of " +this.area;
    }
}