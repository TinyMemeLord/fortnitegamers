public class Trapezoid {
    private double smallBase;
    private double bigBase;
    private double height;
    public Trapezoid(double smallBase,double bigBase,double height){
        this.smallBase = smallBase;
        this.bigBase = bigBase;
        this.height = height;
    }
    public double getArea(){
        return((smallBase+bigBase)/2*height);
    } 
    @Override
    public String toString() {
        return "{ smallBase: "+smallBase+", bigBase: "+bigBase+", height: "+height+" }";
    }
}
