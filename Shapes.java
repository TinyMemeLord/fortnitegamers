public class Shapes{
    public static void main(String[]args){
        Trapezoid trapezoid = new Trapezoid(2,5,6);
        System.out.println(trapezoid);
        System.out.println(trapezoid.getArea());
        Triangle triangle = new Triangle(10,2);
        System.out.println(triangle);
        System.out.println(triangle.getArea());

        Rectangle r = new Rectangle(5,4);
        System.out.println(r);
    }
}